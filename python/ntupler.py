#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse

from xAODAnaHelpers import Config, utils

commonflags={}
commonflags['bkg']='--isMC'
commonflags['sig']='--isMC'
commonflags['fastsim']='--isMC --isAFII'

def parse_types(option):
    types=[]
    for optionkey in option.keys():
        if optionkey.startswith('filelists_'):
            types.append(optionkey[10:])
    return types

def create_argparser(options=None):
    #
    # Determine some useful defaults

    # If you want the output to go to a particular disk, set it here.
    # Set default localgroupdisk to look for
    # Add your username here if you like :)
    # If you add a localGroupDisk from the command line it will
    # overwrite the one here.
    destinations={}
    username = getpass.getuser()
    destinations['kpachal']="CA-SFU-T2_LOCALGROUPDISK"
    destinations['ecorriga']="SE-SNIC-T2_LUND_LOCALGROUPDISK"
    destinations['kkrizka']="NERSC_LOCALGROUPDISK"

    #
    # Create the parser object
    parser = argparse.ArgumentParser(description='Launch the ntuple making process.')
    parser.add_argument('-p','--pretend', action='store_true', help='Print only submission commands.')

    # Add options for running
    if options!=None:
        for key,config in options.items():
            types=['all']
            for configkey in config.keys():
                if configkey.startswith('filelists_'):
                    types.append(configkey[10:])
            parser.add_argument('--{0}'.format(key), metavar=','.join(types), help='Run over listed {config} configurations.'.format(config=key))

    # Specify where to run jobs
    drivers_parser = parser.add_subparsers(prog='runntupler.py', title='drivers', dest='driver', description='specify where to run jobs')

    direct = drivers_parser.add_parser('direct',help='Run your jobs locally.')
    grid = drivers_parser.add_parser('grid',help='Run your jobs on the grid.')
    grid.add_argument('--destination', type=str, required=False, default=destinations.get(username,None))
    grid.add_argument('--tag', type=str, required=True)
    slurm = drivers_parser.add_parser('slurm',help='Run your jobs on SLURM.')

    return parser

def runntupler(options,package='ZprimeDM'):
    parser = create_argparser(options)
    args = parser.parse_args()

    ##-----------------------------------------------##
    ## Organisation and running
    user = getpass.getuser()
    tag = time.strftime("%Y%m%d")
    isPDSF = 'pdsf' in os.uname()[1] and args.driver!='grid'

    #
    # Prepare output directory
    if args.driver=="grid":
        datadir = tempfile.mkdtemp(suffix="{0}_{1}_{2}_{3}".format(user,package,tag,args.driver))
    else :
        datadir = os.environ.get('DATADIR',os.getcwd())

    # Declare options for running with different drivers
    if args.driver=="direct":
        runcode ="--nevents=10000 direct"
    elif args.driver=="grid":
        dest = ""
        if args.destination!=None:
            dest += "--optGridDestSE={0} ".format(args.destination)
        runcode ="prun {dest} --optGridOutputSampleName=user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%.SELECTION.{tag}/".format(dest=dest,tag=args.tag)
    elif args.driver=="slurm":
        pdsfslurm='--optBatchSharedFileSystem=1 --optSlurmAccount="atlas" --optSlurmPartition="shared" --optSlurmRunTime="24:00:00" --optSlurmMemory="1800" --optSlurmExtraConfigLines="#SBATCH --image=custom:pdsf-chos-sl64:v4 --export=NONE" --optSlurmWrapperExec="export TMPDIR=${{SLURM_TMP}}; hostname; shifter --volume=/global/project:/project --volume=/global/projecta:/projecta /bin/bash " --optBatchShellInit="export X509_USER_PROXY={X509_USER_PROXY}; export AtlasSetupSite={AtlasSetupSite}; export AtlasSetup={AtlasSetup}; source {AtlasSetup}/scripts/asetup.sh {AtlasProject},{AtlasVersion}; source {WorkDir_DIR}/setup.sh"'.format(
            AtlasSetupSite=os.environ['AtlasSetupSite'],
            AtlasSetup=os.environ['AtlasSetup'],
            AtlasProject=os.environ['AtlasProject'],
            AtlasVersion=os.environ['AtlasVersion'],
            WorkDir_DIR=os.environ['WorkDir_DIR'],
            X509_USER_PROXY=os.environ['X509_USER_PROXY']
            )
        runcode ="slurm --optBatchWait {pdsf}".format(pdsf=pdsfslurm)
    else :
        print("Unrecognized mode: {0}".format(args.driver))
        return

    # Prepare commands for running
    if utils.is_release20():
        commandFormat = "xAH_run.py --config {package}/data/config_{selection}_ntuple.py --files {files} --inputList {flags} --submitDir {datadir}/{outname} --force {runcode} "
        filelistdir=package+'/filelists/R20p7_lists'
    else:
        commandFormat = "xAH_run.py --config ../{package}/data/config_{selection}_ntuple.py --files {files} --inputList {flags} --submitDir {datadir}/{outname} --force {runcode} "
        filelistdir='../'+package+'/filelists'
    commands = []

    enablealloptions=all([getattr(args,key)==None for key in options.keys()])

    for key,option in options.items():
        print("Beginning option {0}".format(key))

        # Skip any options diabled via argument
        enabled=getattr(args,key)
        if not enabled and not enablealloptions:
            print('\tskip...')
            continue

        # Get list of enabled types
        enabled=enabled.split(',') if not enablealloptions else ['all']
        types=parse_types(option)
        if 'all' not in enabled:
            enabled = filter(lambda x: x in enabled, types)
        else:
            enabled = types

        # Loop over types and prepare a command for each
        for type in enabled:
            # Get any special flags


            # Prepare the filelist
            filelists=['{filelistdir}/{filelist}.{derivation}.list'.format(filelistdir=filelistdir,
                                                                           filelist=filelist,
                                                                           derivation=option['derivation'])
                       for filelist in option['filelists_'+type]]

            if isPDSF: # PDSF hack
                pdsffilelists=[]
                for filelist in filelists:
                    fh=open(filelist)
                    for line in fh:
                        line=line.strip()
                        if line=='': continue
                        pdsffilelists.append('../ZprimeFilelists/filelists/'+line+'.txt')
                    fh.close()
                filelists=pdsffilelists

            files=' '.join(filelists)
            flags=commonflags.get(type,'')
            if not isPDSF: flags+=' --inputRucio'
            outname='OUT_{option}_{type}_ntuple'.format(option=key,
                                                        type=type)
            runcode2=runcode.replace('SELECTION',option['selection'])

            commands.append(commandFormat.format(selection=option['selection'],
                                                 package=package,
                                                 files=files,
                                                 flags=flags,
                                                 datadir=datadir,
                                                 outname=outname,
                                                 runcode=runcode2))
    #
    # Run!
    for command in commands:
        print(command+"\n")
        if not args.pretend: subprocess.check_call(command,shell=True)
        
    #
    # Clean-up
    if args.driver=='grid': subprocess.check_call("rm -rf {0}".format(datadir),shell=True)
