import ROOT
from xAODAnaHelpers import Config

GRL = ','.join(["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
                "GoodRunsLists/data17_13TeV/20171019/data17_13TeV.periodAllYear_DetStatus-v95-pro21-09_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]) 
btagWPs=[60,70,77,85]
btaggers=['DL1rnn','DL1mu','DL1','MV2c10rnn','MV2c10mu','MV2c10']

def apply_common_config(c,isMC=False,isAFII=False,triggerSelection='',
                        doPileupCleaning=False,doSyst=False,
                        doJets=True, doFatJets=False, doTrackJets=False, doPhotons=True, doMuons=False, doElectrons=False,
                        btagWPs=[],
                        msgLevel=ROOT.MSG.INFO):
    if not isMC: # Data Config
        jet_calibSeq = 'JetArea_Residual_EtaJES_GSC_Insitu'
        systName     = ''
        systVal      = 0
        jetSystVal   = ''
    else: # MC Config
        jet_calibSeq = 'JetArea_Residual_EtaJES_GSC'
        systName     = "All" if doSyst else ''
        systVal      = 1 if doSyst else 0
        jetSystVal   = '' if doSyst else '' #"0.5,1,1.5,2,2.5,3" # dijet limit setting requirement

    # AFII
    #
    if isAFII:
            JESUncertMCType = "AFII"
    else:
            JESUncertMCType = "MC15"

    # GoodRunsLists/data16_13TeV/20170215/
    BasicEventSelection = { "m_name"                  : "BasicEventSelection",
                            "m_msgLevel"              : msgLevel,
                            "m_applyGRLCut"           : True,
                            "m_GRLxml"                : GRL,
                            "m_useMetaData"           : True,
                            "m_storeTrigDecisions"    : True,
                            "m_triggerSelection"      : triggerSelection,
                            "m_applyTriggerCut"       : not isMC,
                            "m_PVNTrack"              : 2,
                            "m_applyPrimaryVertexCut" : True,
                            "m_applyEventCleaningCut" : True,
                            "m_applyCoreFlagsCut"     : True,
                            "m_doPUreweighting"       : isMC,
                            "m_PRWFileNames"          : "dev/SUSYTools/merged_prw_mc16a_latest.root,dev/SUSYTools/PRW_mc16a_signals/mc16_13TeV.MGPy8EG_N30LO_A14N23LO_dmA_jja_merged_prw_latest.root",
                            "m_lumiCalcFileNames"     : "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root,GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS_297730-311481_OflLumi-13TeV-008.root,GoodRunsLists/data17_13TeV/20170901/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root",
                            "m_duplicatesStreamName"  : 'dup_tree'
                            }
    if doPileupCleaning and isMC:
        BasicEventSelection["m_MCPileupCheckRecoContainer"] ="AntiKt4EMTopoJets"
        BasicEventSelection["m_MCPileupCheckTruthContainer"]="AntiKt4TruthJets"

    c.algorithm("BasicEventSelection", BasicEventSelection )



    # Setup intermediate names, if needed
    doOR=doJets and doPhotons
    if doOR:
        nameJetContainer     ='Jets_Select'
        namePhotonContainer  ='Photons_Select'
        nameMuonContainer    ='Muons_Select'
        nameElectronContainer='Electrons_Select'
    else:
        nameJetContainer     ='SignalJets'
        namePhotonContainer  ='SignalPhotons'
        nameMuonContainer    ='SignalMuons'
        nameElectronContainer='SignalElectrons'

    if doJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt4EMTopoJets",
                                            "m_outContainerName"        : "Jets_Calib",
                                            "m_outputAlgo"              : "Jets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt4EMTopo",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigAFII"         : "JES_MC15Prerecommendation_AFII_June2015.config",
                                            "m_calibConfigFullSim"      : "JES_data2016_data2015_Recommendation_Dec2016_rel21.config",
                                            "m_calibConfigData"         : "JES_data2016_data2015_Recommendation_Dec2016_rel21.config",
                                            "m_calibSequence"           : jet_calibSeq,
                                            "m_setAFII"                 : isAFII,
                                            "m_JESUncertConfig"         : "JES_2016/Moriond2017/JES2016_SR_Scenario1.config",
                                            "m_JESUncertMCType"         : JESUncertMCType,
                                            "m_JERUncertConfig"         : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
                                            "m_JERFullSys"              : False,
                                            "m_JERApplyNominal"         : False,
                                            "m_redoJVT"                 : False,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_systValVectorString"     : jetSystVal
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Jets_Calib",
                                            "m_inputAlgo"               : "Jets_Calib_Algo",
                                            "m_outContainerName"        : nameJetContainer,
                                            "m_outputAlgo"              : nameJetContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 25e3,
                                            "m_eta_max"                 : 2.8,
                                            "m_useCutFlow"              : True,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : True,
                                            "m_WorkingPointJVT"         : "Medium",
                                            "m_SFFileJVT"               : "JetJvtEfficiency/Moriond2017/JvtSFFile_EM.root"
                                            } )
        for btagWP in btagWPs:
            btagWP="HybBEff_%d"%btagWP
            for tagger in btaggers:
                c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_"+nameJetContainer+"_"+tagger+"_"+btagWP,
                                                         "m_msgLevel"                : msgLevel,
                                                         "m_inContainerName"         : nameJetContainer,
                                                         "m_inputAlgo"               : nameJetContainer+"_Algo",
                                                         "m_systName"                : systName,
                                                         "m_systVal"                 : systVal,
                                                         "m_operatingPt"             : btagWP,
                                                         "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2017-07-02_v1.root",
                                                         "m_jetAuthor"               : "AntiKt4EMTopoJets",
                                                         "m_taggerName"              : tagger,
                                                         "m_decor"                   : "BTag"
                                                         } )


    #
    # Fat jets
    if doFatJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                                            "m_outContainerName"        : "FatJets_Calib",
                                            "m_outputAlgo"              : "FatJets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigFullSim"      : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights_rel21.config",
                                            "m_calibConfigData"         : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights_rel21.config",
                                            "m_calibSequence"           : 'EtaJES_JMS',
                                            "m_setAFII"                 : isAFII,
                                            "m_JESUncertConfig"         : "$ROOTCOREBIN/data/JetUncertainties/UJ_2016/Moriond2017/UJ2016_CombinedMass_strong.config",
                                            "m_JESUncertMCType"         : JESUncertMCType,
                                            "m_jetCleanCutLevel"        : "LooseBad",
                                            "m_jetCleanUgly"            : True,
                                            "m_cleanParent"             : True,
                                            "m_applyFatJetPreSel"       : True,    # make sure fat-jet uncertainty is applied only in valid region
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_systValVectorString"     : jetSystVal
                                            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "FatJets_Calib",
                                            "m_inputAlgo"               : "FatJets_Calib_Algo",
                                            "m_outContainerName"        : "SignalFatJets",
                                            "m_outputAlgo"              : "SignalFatJets_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_cleanJets"               : True,
                                            "m_pT_min"                  : 250e3,
                                            "m_eta_max"                 : 2.0,
                                            "m_useCutFlow"              : True
                                            } )
    #
    # Track Jets
    if doTrackJets:
        c.algorithm("JetSelector", { "m_name"                    : "SelectTrackJets",
                                     "m_inContainerName"         : "AntiKt2PV0TrackJets",
                                     "m_outContainerName"        : "SignalTrackJets",
                                     "m_decorateSelectedObjects" : False,
                                     "m_createSelectedContainer" : True,
                                     "m_cleanJets"               : True,
                                     "m_pT_min"                  : 10e3,
                                     "m_eta_max"                 : 2.5,
                                     "m_useCutFlow"              : True,
                                     "m_doJVF"                   : False
                                     } )


        for btagWP in btagWPs:
            btagWP="HybBEff_%d"%btagWP
            for tagger in btaggers:
                c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalTrackJets_"+tagger+"_"+btagWP,
                                                         "m_msgLevel"                : msgLevel,
                                                         "m_inContainerName"         : "SignalTrackJets",
                                                         "m_systName"                : systName,
                                                         "m_systVal"                 : systVal,
                                                         "m_operatingPt"             : btagWP,
                                                         "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2017-07-02_v1.root",
                                                         "m_jetAuthor"               : "AntiKt4EMTopoJets",
                                                         "m_taggerName"              : tagger,
                                                         "m_decor"                   : "BTag",
                                                         "m_outputSystName"          : "BTag_TrackJets"
                                                         } )

    #
    # Photons
    if doPhotons:
        c.algorithm("PhotonCalibrator",   { "m_name"                    : "CalibratePhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons",
                                            "m_outContainerName"        : "Photons_Calib",
                                            "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                            "m_photonCalibMap"          : "PhotonEfficiencyCorrection/2015_2016/rel20.7/Moriond2017_v1/map0.txt",
                                            "m_tightIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf",
                                            "m_mediumIDConfigPath"      : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf",
                                            "m_looseIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf",
                                            "m_esModel"                 : "es2016data_mc15c",
                                            "m_decorrelationModel"      : "1NP_v1",
                                            "m_useAFII"                 : isAFII,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_sort"                    : True
                                            } )

        c.algorithm("PhotonSelector",     { "m_name"                    : "SelectPhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons_Calib",
                                            "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                                            "m_outContainerName"        : namePhotonContainer,
                                            "m_outputAlgoSystNames"     : namePhotonContainer+"_Algo",
                                            "m_decorateSelectedObjects" : True,
                                            "m_createSelectedContainer" : True,
                                            "m_pT_min"                  : 10e3,
                                            "m_eta_max"                 : 2.37,
                                            "m_vetoCrack"               : True,
                                            "m_doAuthorCut"             : True,
                                            "m_doOQCut"                 : True,
                                            "m_photonIdCut"             : "Tight",
                                            "m_MinIsoWPCut"             : "FixedCutTightCaloOnly"
                                            } )

    #
    # Muons
    if doMuons:
        c.algorithm("MuonCalibrator", { "m_name"                   : "CalibrateMuons",
                                        "m_msgLevel"               : msgLevel,
                                        "m_inContainerName"        : "Muons",
                                        "m_outContainerName"       : "Muons_Calib",
                                        "m_outputAlgoSystNames"    : "Muons_Calib_Algo",
                                        "m_Years"                  : "Data15,Data16",
                                        "m_sagittaRelease"         : "sagittaBiasDataAll_25_07_17",
                                        "m_do_sagittaCorr"         : True,
                                        "m_do_sagittaMCDistortion" : False,
                                        "m_release"                : "Recs2016_15_07",
                                        "m_forceDataCalib"         : True
                                        } )

        c.algorithm("MuonSelector", { "m_name"                    : "SelectMuons", 
                                      "m_inContainerName"         : "Muons_Calib", 
                                      "m_outContainerName"        : nameMuonContainer, 
                                      "m_createSelectedContainer" : True,
                                      "m_pT_min"                  : 10e3,
                                      "m_eta_max"                 : 2.5,
                                      "m_muonQualityStr"          : "Medium",
                                      "m_d0sig_max"               : 3,
                                      "m_z0sintheta_max"          : 1.0,
                                      "m_MinIsoWPCut"             : "Loose"
                                      } )

    #
    # Electrons
    if doElectrons:
        c.algorithm("ElectronCalibrator", { "m_name"                : "Electrons",
                                            "m_inContainerName"     : "Electrons",
                                            "m_outContainerName"    : "Electrons_Calib",
                                            "m_outputAlgoSystNames" : "Electrons_Calib_Algo",
                                            "m_esModel"             : "es2017_R21_PRE",
                                            "m_decorrelationModel"  : "FULL_v1"
                                            })

        c.algorithm("ElectronSelector", { "m_name"                    : "SelectElectrons",
                                          "m_inContainerName"         : "Electrons_Calib",
                                          "m_inputAlgoSystNames"      : "Electrons_Calib_Algo",
                                          "m_outContainerName"        : nameElectronContainer,
                                          "m_outputAlgoSystNames"     : nameElectronContainer+"_Algo",
                                          "m_createSelectedContainer" : True,
                                          "m_doLHPID"                 : True,
                                          "m_doLHPIDcut"              : True,
                                          "m_LHOperatingPoint"        : "Loose",
                                          "m_MinIsoWPCut"             : "LooseTrackOnly",
                                          "m_d0sig_max"               : 5.,
                                          "m_z0sintheta_max"          : 0.5,
                                          "m_pT_min"                  : 7*1000.,
                                          "m_eta_max"                 : 2.47
                                          })

    if doOR:
        OverlapRemover={ "m_name"                    : "RemoveOverlaps",
                         "m_outputAlgoSystNames"     : "ORAlgo_Syst",
                         "m_createSelectedContainers": True}
        if doJets:
            OverlapRemover["m_inContainerName_Jets"]      =nameJetContainer
            OverlapRemover["m_inputAlgoJets"]             =nameJetContainer+"_Algo"
            OverlapRemover["m_outContainerName_Jets"]     ="SignalJets"
        if doPhotons:
            OverlapRemover["m_inContainerName_Photons"]   =namePhotonContainer,
            OverlapRemover["m_inputAlgoPhotons"]          =namePhotonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Photons"]  ="SignalPhotons"
        if doMuons:
            OverlapRemover["m_inContainerName_Muons"]     =nameMuonContainer,
            OverlapRemover["m_inputAlgoMuons"]            =nameMuonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Muons"]    ="SignalMuons"
        if doElectrons:
            OverlapRemover["m_inContainerName_Electrons"] =nameElectronContainer,
            OverlapRemover["m_inputAlgoElectrons"]        =nameElectronContainer+"_Algo"
            OverlapRemover["m_outContainerName_Electrons"]="SignalElectrons"

        c.algorithm("OverlapRemover", OverlapRemover)


def findAlgo(c,name):
    for algo in c._algorithms:
        if algo.m_name==name: return algo
    return None
