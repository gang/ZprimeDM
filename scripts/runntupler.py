#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse

from ZprimeDM import ntupler

##-----------------------------------------------##
## Users: set sample information here

#
# Mapping of different options to run. Each option has
# the following attributes:
#  selection: name of ntupler configuration to run (SELECTION from config_SELECTION_ntuple.py)
#  filelists_type: list of filelists (FILELIST from FILELIST.deriv.list) for type
#                  type can be any one of data, bkg, sig or fastsim
#  derivation: name of derivation to use for the filelists (DERIV from filelist.DERIV.list)
#  
optionDict = {
    "test" :  {'selection': 'trijet',
               'derivation':'EXOT2',
               'filelists_bkg':["gridTest_trijet.mc",
                                ],
               'filelists_data':["gridTest_trijet.2017",
                                 ]
               },

    "trijet" :  {'selection': 'trijet',
                 'derivation':'EXOT2',
                 'filelists_bkg':["Pythia8_dijet",
                                  "Sherpa_dijet",
                                  ],
                 'filelists_sig':["Signal_trijet",
                                  ],
                 'filelists_data':["data15",
                                   "data16",
                                   "data17",
                                   ]
                 },

    "dijetgamma" :  {'selection': 'dijetgamma',
                     'derivation':'EXOT6',
                     'filelists_bkg':["Sherpa_gammajet",
                                      ],
                     'filelists_sig':["Signal_dijetgamma",
                                      ],
                     'filelists_data':["data15",
                                       "data16",
                                       "data17",
                                       ]
                     },

    }

## End of user specified information.

#### RUN EVERYTHING ####
ntupler.runntupler(optionDict)
