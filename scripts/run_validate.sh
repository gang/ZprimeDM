#!/bin/bash

function abortError {
    echo "FAILED ${1}"
    exit -1
}

if [ ${#} != 1 ]; then
    echo "usage: ${0} direct/condor"
    exit -1
fi
MODE=${1}

if [ "x${MODE}x" == "xcondorx" ]; then
    RUNCODE="condor --optFilesPerWorker 10 --optBatchWait"
else
    RUNCODE="direct"
fi

FILES_JETJETJET="ZprimeDM/filelists/MC15.99995*.MGPy8EG_dmA_dijet_Np*_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"
FILES_JETJETJET_NOMERG="ZprimeDM/filelists/MC15.99994*.MGPy8EG_dmA_dijet_Np*_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"
FILES_JETJETJET_ATHFILT="ZprimeDM/filelists/MC15.99990*.MGPy8EG_dmA_dijet_Np*_Jet*_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"
FILES_GAMMAJETJET="ZprimeDM/filelists/MC15.999980.MGPy8EG_dmA_dijetgamma_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"
FILES_GAMMAJETJET_MGFILT="ZprimeDM/filelists/MC15.999890.MGPy8EG_dmA_dijetgamma_Ph*_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"
FILES_GAMMAJETJET_ATHFILT="ZprimeDM/filelists/MC15.999880.MGPy8EG_dmA_dijetgamma_Ph*_mR*_mDM10000_gSM*_gDM1p50.TRUTH1.txt"

#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_validation_trijet.py --files ${FILES_JETJETJET} ${FILES_JETJETJET_NOMERG} ${FILES_JETJETJET_ATHFILT} --inputList --submitDir OUT_validate_trijet --force ${RUNCODE}
./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_validation_trijet.py --files ${FILES_JETJETJET} ${FILES_JETJETJET_ATHFILT} --inputList --submitDir OUT_validate_trijet --force ${RUNCODE}
#./xAODAnaHelpers/scripts/xAH_run.py --config ZprimeDM/data/config_validation_gammajetjet.py --files ${FILES_GAMMAJETJET} ${FILES_GAMMAJETJET_MGFILT} ${FILES_GAMMAJETJET_ATHFILT} --inputList --submitDir OUT_validate_dijetgamma --force ${RUNCODE}
