import ROOT

inputFileName = "TestTrigStudy/hist-tree.root"

inFile = ROOT.TFile(inputFileName,"READ")

class photonCounts: 
    def __init__(self, name):
        self.name = name
        self.jet0 = -1
        self.jet1 = -1
        self.jet2 = -1

    def num(self, inNumber):
        return round(inNumber,2)

    def printline(self, number, scalingToHz):
        print "\t",self.num(number* scalingToHz),"$\\pm$",self.num(pow(number,0.5)* scalingToHz),"Hz",

    def Print(self, scalingToHz):
        print self.name.replace("_","\\\_"),
        
        doLatex = True
        if doLatex:

            print "&",
            self.printline(self.jet0, scalingToHz),
            print "&",
            self.printline(self.jet1, scalingToHz),
            print "&",
            self.printline(self.jet2, scalingToHz),
            print "\\\\\\\\",

        else:
            print "\t",self.num(self.jet0* scalingToHz),"+/-",self.num(pow(self.jet0,0.5)* scalingToHz),"Hz",
            print "\t",self.num(self.jet1* scalingToHz),"+/-",self.num(pow(self.jet1,0.5)* scalingToHz),"Hz",
            print "\t",self.num(self.jet2* scalingToHz),"+/-",self.num(pow(self.jet2,0.5)* scalingToHz),"Hz",
        print


def getCount(dirName, jetCut, nJetCut):

    jetBin = nJetCut+1

    jetHist = inFile.Get(dirName+"/nJet"+jetCut)
    count = jetHist.Integral(jetBin,10)
    return count

def getJetNumbers(dirName, jetCut, trigCounts):
    trigCounts.jet0 = getCount(dirName, jetCut, 0)
    trigCounts.jet1 = getCount(dirName, jetCut, 1)
    trigCounts.jet2 = getCount(dirName, jetCut, 2)
    return 
    

def getThresholdNumbers(thrsName, jetCut, scalingToHz, doPrint=True):

    if doPrint and False:
        print 
        print thrsName
        print "---------"
    looseCounts = photonCounts(thrsName+"_loose")
    getJetNumbers(thrsName+"_loose", jetCut, looseCounts)

    tightCounts     = photonCounts(thrsName+"_Tight")
    getJetNumbers(thrsName+"_Tight", jetCut, tightCounts)

    tightIsoCounts  = photonCounts(thrsName+"_TightIso")
    getJetNumbers(thrsName+"_TightIso", jetCut, tightIsoCounts)

    if doPrint:
        looseCounts   .Print(scalingToHz)
        tightCounts   .Print(scalingToHz)
        tightIsoCounts.Print(scalingToHz)
        print
        print

    return looseCounts, tightCounts, tightIsoCounts
        

InputRate = 2.3177*187.00 * 3.20
Scaling   = InputRate / getThresholdNumbers("g35","50",1.0,doPrint=False)[0].jet0

print "Input rate",InputRate
print Scaling

print "25 GeV Jets "
print "=================="
print "\t\tNJet>=0\t\t\tNJet>=1\t\t\tNJet>=2"
print "\t\t-----------------------------------------------------------------"
getThresholdNumbers("g35", "25",  Scaling)
getThresholdNumbers("g45", "25",  Scaling)
getThresholdNumbers("g55", "25",  Scaling)
getThresholdNumbers("g65", "25",  Scaling)
getThresholdNumbers("g75", "25",  Scaling)
print
print

print "50 GeV Jets "
print "=================="
print "\t\tNJet>=0\t\t\tNJet>=1\t\t\tNJet>=2"
print "\t\t-----------------------------------------------------------------"
getThresholdNumbers("g35", "50",  Scaling)
getThresholdNumbers("g45", "50",  Scaling)
getThresholdNumbers("g55", "50",  Scaling)
getThresholdNumbers("g65", "50",  Scaling)
getThresholdNumbers("g75", "50",  Scaling)
print 
print




