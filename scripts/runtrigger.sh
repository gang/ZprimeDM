#!/bin/bash

source ZprimeDM/scripts/librun.sh

# sample list
MC_DIJETGAMMA="ZprimeFilelists/filelists/user.kkrizka.mc15_13TeV.*.Sherpa_CT10_SinglePhotonPt*.photontrigger.20170301-01_tree.root.list"
DATA_DIJETGAMMA="ZprimeFilelists/filelists/user.kkrizka.data1*.period*.photontrigger.20170309-01_tree.root.list"

# Process
RUNLIST=""

#
# Trigger studies
runOne photontrigger photontrigger_mc "-m -l" ${MC_DIJETGAMMA}
runOne photontrigger photontrigger_data "-l" ${DATA_DIJETGAMMA}

for RUN in ${RUNLIST}
do
    echo "Waiting for PID ${RUN}"
    wait ${RUN} || exit 1
    echo "COMPLETED WITH ${?}"
done

#
# Merge
#

#
# Trigger studies
merge photontrigger_mc   hist.root "hist-*Sherpa*SinglePhoton*root"

merge photontrigger_data hist15.root "hist-*data15*root"
merge photontrigger_data hist16.root "hist-*data16*root"
merge photontrigger_data hist.root   "hist-*data1[5-6]*root"
