#!/bin/bash

if [ ${#} != 3 ]; then
    echo "usage: ${0} dslist selection date"
    exit -1
fi

dslist=${1}
selection=${2}
date=${3}

outlist=$(dirname ${dslist})/$(basename ${dslist} | cut -f 1 -d '.').${selection}.NTUP.list
echo ${outlist}

if [ -e ${outlist} ]; then
    rm ${outlist}
fi

for dsname in $(cat ${dslist})
do
    echo ${dsname}

    if [[ "${dslist}" == *".cont."* ]]; then
	dsprefix=$(echo ${dsname} | cut -f 3-4 -d '.')
    else
	dsprefix=$(echo ${dsname} | cut -f -3 -d '.')
    fi
	rucio list-dids user.kkrizka.${dsprefix}.${selection}.${date}_tree.root/ | grep CONTAINER | awk '{print $2}' | cut -f 2 -d ':' >> ${outlist}
done
