import os,sys
import subprocess
import tempfile
import getpass

def abortError () :
    print "FAILED",sys.argv[1]
    sys.exit(-1)

if (len(sys.argv) < 2) :
  print "usage: {0} direct/condor/lsf/pbs/grid".format(sys.argv[0])
  print "optional: --tag [tag to use]"
  sys.exit(-1)

# Set tests to run.
# These can be overwritten from the command line using --options
# For example:
# --options truth reco data
# Options are: "test", "truth", "reco", "data", "AFII", "signal"
#options = ["signal","data","reco"]
options = ["reco"]

#
# Input dataset lists.
# This format makes it easy to turn off collections we don't need right now.
# Specify which list of samples to pick up for each set of files.
fileListDict = {
"truth" : [],
"reco" :  [
           "Sherpa_dijetgamma",
#           "Pythia8_trijet",
#           "Pythia8_trijet_JZ",
#           "Sherpa_trijet"
          ],
"data" :  [
#           "data15_dijetgamma",
#           "data15_trijet",
#           "data16_dijetgamma",
#           "data16_trijet",
#           "data17_dijetgamma",
#           "data17_trijet"
          ],
"AFII" :  [],
"signal" : [
           "Signal_dijetgamma",
           "Signal_trijet",
]
}

# Prepare dictionary for config naming.
# This will be appended to end of standard config if
# something other than default is required:
# i.e. "config_dijetgamma_ntuple.py" -> "config_dijetgamma_ntuple_truth.py"
# So if you make a new config, add specifications here.
configExtensions = {
"truth" : "truth",
"reco" : "",
"data" : "",
"triggerStudies" : "",
"AFII" : "",
"signal" : ""
}

# Any special required flags for running can be set here.
# Usually these should not need to be changed.
specialFlags = {
"truth" : "--isMC",
"reco" : "--isMC",
"data" : "",
"triggerStudies" : "",
"AFII" : "--isMC --isAFII",
"signal" : "--isMC"
}

# An example script for a PBS file that works on your batch.
templatescript = "batchScript_template.sh"
scriptArchive = "../batchconfigs/"

## End of user specified information.

##-----------------------------------------------##
## Allow batch running in PBS, which

def batchSetup(command,stringForNaming) :

  # Perform setLimitsOneMassPoint on batch
  batchcommand = command.split("|&")[0]
  print batchcommand

  # Open batch script as fbatchin
  fbatchin = open(templatescript, 'r')
  fbatchindata = fbatchin.read()
  fbatchin.close()

  # open modified batch script (fbatchout) for writing
  batchtempname = '{0}/batchSubmit_{1}.sh'.format(scriptArchive,stringForNaming)
  fbatchout = open(batchtempname,'w')
  fbatchoutdata = fbatchindata.replace("YYY",os.getcwd())
  fbatchoutdata = fbatchoutdata.replace("ZZZ",batchcommand) # In batch script replace ZZZ for submit command
  fbatchout.write(fbatchoutdata)
  modcommand = 'chmod 744 {0}'.format(batchtempname)
  subprocess.call(modcommand, shell=True)
  fbatchout.close()
  submitcommand = "qsub {0}".format(batchtempname)
  return submitcommand

##-----------------------------------------------##
## Organisation and running

mode = sys.argv[1]
tag = "hist"
if len(sys.argv) > 2 :
  tag = sys.argv[2]

#
# Prepare output directory
if "grid" in mode :
  user = getpass.getuser()
  datadir = tempfile.mkdtemp(suffix="{0}_ZPrime_DM_{1}_{2}".format(user,tag,mode))
else :
  datadir = os.getcwd()+"/../results/"

# Declare options for running with different drivers
# runcode is set up for running on private rucio containers
# since this is what we will (afaik) always be dealing with
# at the histogram stage

if ( "grid" in mode ) :

  runcode = "prun --optGridOutputSampleName=user.%nickname%.%in:name[3]%.%in:name[4]%.SELECTION.{0}/".format(tag)
elif ( "condor" in mode ) :
  runcode = "condor --optFilesPerWorker 5 --optBatchWait"
elif ( "lsf" in mode ) :
  runcode = "lsf "
elif ("direct" or "pbs" in mode) :
  runcode = "direct "
else :
  print "Unrecognized mode",mode

# Prepare commands for running
commandFormat = "xAH_run.py --config ZprimeDM/data/{0} --files {1} --inputList --inputRucio {5} --submitDir {2}/{3} --force {4} "
commands = []

for option in options :

  print "Beginning option",option

  # Go through individual files and set
  # up commands to run using info from config dictionaries.
  useFiles = fileListDict[option]
  print "useFiles is",useFiles
  configExtension = "" if not configExtensions[option] else "_{0}".format(configExtensions[option])
  flags = specialFlags[option]

  filedir = "ZprimeDM/filelists/"

  # For the batch, we want one job per input container, to parallelise as much
  # as possible. This will be handled for condor or lsf, but we are doing pbs ourselves:
  if "pbs" in mode :

    for list in useFiles :
      print "Beginning type",list
      channel = list.split("_")[1]
      filelist = filedir + "{0}.R21.NTUP.list".format(list)
      config = "config_{0}{1}.py".format(channel,configExtension)
 
      with open(filelist) as f :
        for line in f :
          file = line.strip()
          tokens = file.split(".")
          print "tokens:",tokens
          dirName = "batchOut_{0}_{1}_{2}".format(channel,option,tokens[4])
          innercommand = commandFormat.format(config, filelist, datadir, dirName, runcode, flags)
          command = batchSetup(innercommand,"{0}_{1}_{2}".format(channel,option,tokens[4]))
          commands.append(command)

    continue

  # Tidiest thing on grid or locally is to submit everything with same run options together.
  # That will be everything sharing a channel. Make a filelist and index
  # by channel to sort these. The built in batch drivers will take care of sensible parallelisation.
  fileLists = {}
  for list in useFiles :
    print "Beginning type",list
    channel = list.split("_")[1]
    filelist = filedir + "{0}.R21.NTUP.list".format(list)
    if channel in fileLists.keys() :
      fileLists[channel] = fileLists[channel] + " " + filelist
    else :
      fileLists[channel] = filelist

  # Now we can submit only one job for each setup.
  for channel,filelist in fileLists.iteritems() :

    code = runcode.replace("SELECTION","{0}.{1}".format(channel,option))
    dirName = "OUT_{0}_{1}".format(channel,option)
    config = "config_{0}{1}.py".format(channel,configExtension)

    command = commandFormat.format(config, filelist, datadir, dirName, code, flags)
    commands.append(command)

#
# Run!
for command in commands :
  print command+"\n"
  subprocess.call(command,shell=True)

#
# Clean-up
subprocess.call("rm -rf DATADIR",shell=True)
