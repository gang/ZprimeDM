##
## Period J1 (Proton-proton-physics.)
##
data15_13TeV.00282625.physics_Main.merge.AOD.f640_m1511
data15_13TeV.00282631.physics_Main.merge.AOD.f640_m1511
data15_13TeV.00282712.physics_Main.merge.AOD.f640_m1511
##
## Period J2 (Runs with 8b4e filling scheme.)
##
data15_13TeV.00282784.physics_Main.merge.AOD.f640_m1511
#
##
## Period J3 (Runs with 1813 and 2029 bunches.)
##
data15_13TeV.00282992.physics_Main.merge.AOD.f640_m1511
data15_13TeV.00283074.physics_Main.merge.AOD.f640_m1511
data15_13TeV.00283155.physics_Main.merge.AOD.f640_m1511
data15_13TeV.00283270.physics_Main.merge.AOD.f640_m1511
#
#
##
## Period J4 (Runs with 2029 and 2232 colliding bunches. LHC made modifications to the filling scheme.)
##
data15_13TeV.00283429.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00283608.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00283780.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284006.physics_Main.merge.AOD.f643_m1518
#
##
## Period J5 (Fill for BCMS studies, 589 colliding bunches.)
##
data15_13TeV.00284154.physics_Main.merge.AOD.f643_m1518
#
##
## Period J6
##
data15_13TeV.00284213.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284285.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284420.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284427.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284473.physics_Main.merge.AOD.f643_m1518
data15_13TeV.00284484.physics_Main.merge.AOD.f644_m1518
