#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <PathResolver/PathResolver.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/MiniTreeEventSelection.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(MiniTreeEventSelection)

MiniTreeEventSelection :: MiniTreeEventSelection (const std::string& className) :
  Algorithm(className),
  m_mc(false),
  m_triggerDetailStr(""), m_jetDetailStr(""), m_photonDetailStr(""), m_fatjetDetailStr(""), m_truthDetailStr(""), m_trigJetDetailStr(""),
  m_grl(nullptr),
  m_pileup_tool_handle("CP::PileupReweightingTool/PileupToolName", nullptr)
{
  Info("MiniTreeEventSelection()", "Calling constructor");

  // GRL
  m_applyGRL = false;
  m_GRLxml = ""; // Removing default to ensure this always gets set correctly

  // Pileup Reweighting
  m_doPUreweighting    = false;
  m_lumiCalcFileNames  = "";
  m_PRWFileNames       = "";
  m_PU_default_channel = 0;
}

EL::StatusCode MiniTreeEventSelection :: setupJob (EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: histInitialize ()
{
  ANA_MSG_INFO("MiniTreeEventSelection::histInitialize()");

  //
  // data model
  m_eventData=DijetISREvent::global();
  m_eventData->m_doTruthOnly=m_doTruthOnly;
  m_eventData->m_mc=m_mc;

  m_eventData->setTriggerDetail(m_triggerDetailStr);
  if(!m_jetDetailStr.empty())     m_eventData->initializeJets    ("jet"    ,m_jetDetailStr);
  if(!m_photonDetailStr.empty())  m_eventData->initializePhotons ("ph"     ,m_photonDetailStr);
  if(!m_fatjetDetailStr.empty())  m_eventData->initializeFatJets ("fatjet" ,m_fatjetDetailStr);
  if(!m_truthDetailStr.empty())   m_eventData->initializeTruth   ("truth"  ,m_truthDetailStr);
  if(!m_trigJetDetailStr.empty()) m_eventData->initializeTrigJets("jetTrig",m_trigJetDetailStr);

  //
  // Cutflow
  m_cutflow=new CutflowHists("", "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_init    =m_cutflow->addCut("init");
  m_cf_grl     =m_cutflow->addCut("grl");
  m_cutflow->record(wk());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: changeInput (bool firstFile)
{
  ANA_MSG_INFO("MiniTreeEventSelection::changeInput(" << firstFile << ")");

  //
  // Update cutflow hists
  TFile* inputFile = wk()->inputFile();

  TH1D* MetaData_EventCount=dynamic_cast<TH1D*>(inputFile->Get("MetaData_EventCount_Test"));
  if(!MetaData_EventCount)
    {
      Error("MiniTreeEventSelection::changeInput()","Missing input event count histogram!");
      return EL::StatusCode::FAILURE;
    }

  float totalEvents= MetaData_EventCount->GetBinContent(1);
  std::cout << "\ttotalEvents = " << totalEvents << std::endl;

  float totalWeight= MetaData_EventCount->GetBinContent(3);
  std::cout << "\ttotalWeight = " << totalWeight << std::endl;

  m_cutflow->executeInitial(totalEvents, totalWeight);

  //
  // Prepare the branches
  TTree *tree = wk()->tree();
  m_eventData->setTree(tree);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: initialize ()
{
  // 1.
  // initialize the GoodRunsListSelectionTool
  //

  if(m_applyGRL)
    {
      m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
      std::vector<std::string> vecStringGRL;
      std::istringstream f(m_GRLxml);
      std::string GRLxml;
      while(std::getline(f, GRLxml, ','))
	{
	  ANA_MSG_INFO("Adding GRL: " << PathResolverFindCalibFile( GRLxml.c_str() ));
	  vecStringGRL.push_back(PathResolverFindCalibFile( GRLxml.c_str() ));
	}
      ANA_CHECK(m_grl->setProperty("GoodRunsListVec", vecStringGRL));
      ANA_CHECK(m_grl->setProperty("PassThrough"    , false));
      ANA_CHECK(m_grl->initialize());
    }

  // 2.
  // initialize the CP::PileupReweightingTool
  //

  if ( m_doPUreweighting ) {

    std::vector<std::string> PRWFiles;
    std::vector<std::string> lumiCalcFiles;

    // Parse all comma seperated files
    //
    std::string token;
    std::istringstream ss("");

    Info("initialize()", "Adding Pileup files for CP::PileupReweightingTool:");
    ss.clear(); ss.str(m_PRWFileNames);
    while(std::getline(ss, token, ','))
      {
	PRWFiles.push_back(token);
	printf( "\t %s \n", token.c_str() );
      }

    Info("initialize()", "Adding LumiCalc files for CP::PileupReweightingTool:");
    ss.clear(); ss.str(m_lumiCalcFileNames);
    while(std::getline(ss, token, ','))
      {
	lumiCalcFiles.push_back(token);
	printf( "\t %s \n", token.c_str() );
    }

    ANA_CHECK(checkToolStore<CP::PileupReweightingTool>("Pileup"));
    // Based on twiki https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT
    // this command should live outside an ANA_CHECK:
    m_pileup_tool_handle.setTypeAndName("CP::PileupReweightingTool/Pileup");
    ANA_CHECK(m_pileup_tool_handle.setProperty("ConfigFiles",         PRWFiles));
    ANA_CHECK(m_pileup_tool_handle.setProperty("LumiCalcFiles",       lumiCalcFiles));
    if ( m_PU_default_channel ) {
      ANA_CHECK(m_pileup_tool_handle.setProperty("DefaultChannel",    m_PU_default_channel));
    }
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactor",     1.0/1.16));
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactorUP",   1.0));
    ANA_CHECK(m_pileup_tool_handle.setProperty("DataScaleFactorDOWN", 1.0/1.23));
    ANA_CHECK(m_pileup_tool_handle.initialize());
    //m_pileup_tool_handle->EnableDebugging(true);

  }

  Info("initialize()", "Succesfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MiniTreeEventSelection :: execute ()
{
  ANA_MSG_DEBUG("Processing Event");

  // EDM
  wk()->tree()->GetEntry (wk()->treeEntry());
  m_eventData->updateEntry();

  //------------------------------------------------------------------------------------------
  // Update Pile-Up Reweighting
  //------------------------------------------------------------------------------------------
  if ( m_mc && m_doPUreweighting ) {
    m_eventData->m_weight_pileup=m_pileup_tool_handle->expert()->GetCombinedWeight( m_eventData->m_runNumber,
										    m_eventData->m_mcChannelNumber,
										    m_eventData->m_averageInteractionsPerCrossing );
  }

  //
  // Cuts
  float eventWeight    =m_eventData->m_weight * m_eventData->m_weight_pileup;

  m_cutflow->execute(m_cf_init, eventWeight);

  //------------------------------------------------------
  // If data, check if event passes GRL
  //------------------------------------------------------
  if(!m_mc && m_applyGRL)
    {
      if ( !m_grl->passRunLB( m_eventData->m_runNumber, m_eventData->m_lumiBlock ) ) 
	{
	  ANA_MSG_DEBUG("Fail GRL");

	  wk()->skipEvent();
	  return EL::StatusCode::SUCCESS; // go to next event
	}
    }
  m_cutflow->execute(m_cf_grl, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MiniTreeEventSelection :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MiniTreeEventSelection :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  return EL::StatusCode::SUCCESS;
}

