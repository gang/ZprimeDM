#include <ZprimeDM/BCIDBugChecker.h>

#include <fstream>
#include <iostream>

BCIDBugChecker::BCIDBugChecker()
{ }

BCIDBugChecker::~BCIDBugChecker()
{ }

void BCIDBugChecker::addList(const std::string& path)
{
  std::ifstream fh(path);
  uint runNumber, eventNumber;
  if (fh.is_open())
    {
      while(fh >> runNumber >> eventNumber)
	{
	  std::pair<uint,uint> key=std::make_pair(runNumber,eventNumber);
	  m_badEvents.insert(key);
	}
      fh.close();
    }
}

bool BCIDBugChecker::check(uint runNumber, uint eventNumber) const
{
  std::pair<uint,uint> key=std::make_pair(runNumber,eventNumber);
  return m_badEvents.find(key)!=m_badEvents.end();
}

