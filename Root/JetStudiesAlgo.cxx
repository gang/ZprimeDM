#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <ZprimeDM/JetStudiesAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(JetStudiesAlgo)

JetStudiesAlgo :: JetStudiesAlgo () :
  m_mc(false),
  m_jetDetailStr(""),
  m_doPUReweight(false),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_doTrigger(false),
  m_trigger(""),
  m_jet0PtCut(25.),
  m_jet1PtCut(25.),
  m_minPt(-1),
  m_maxPt(-1),
  m_minEta(-1),
  m_maxEta(-1),
  hJet(nullptr)
{
  ANA_MSG_INFO("JetStudiesAlgo::JetStudiesAlgo()");
}

EL::StatusCode JetStudiesAlgo :: histInitialize ()
{
  ANA_CHECK(xAH::Algorithm::algInitialize());
  ANA_MSG_INFO("JetStudiesAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger =m_cutflow->addCut("trigger");
  m_cf_cleaning=m_cutflow->addCut("cleaning");
  m_cf_jet1    =m_cutflow->addCut("jet1");
  m_cf_jet0    =m_cutflow->addCut("jet0");

  if(m_minPt>-1)  m_cf_minpt  =m_cutflow->addCut("minpt");
  if(m_maxPt>-1)  m_cf_maxpt  =m_cutflow->addCut("maxpt");

  if(m_minEta>-1) m_cf_mineta =m_cutflow->addCut("mineta");
  if(m_maxEta>-1) m_cf_maxeta =m_cutflow->addCut("maxeta");

  m_cutflow->record(wk());

  //
  // Histograms
  hJet      =new ZprimeDM::JetHists(m_name+"jet", m_jetDetailStr);
  ANA_CHECK(hJet->initialize());
  hJet->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetStudiesAlgo :: initialize ()
{
  ANA_MSG_DEBUG("JetStudiesAlgo::initialize()");

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetStudiesAlgo :: execute ()
{
  ANA_MSG_DEBUG("JetStudiesAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      ANA_MSG_DEBUG("Doing Trigger");

      //
      // trigger
      bool passTrigger = (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), m_trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, eventWeight);
    }

  //
  // doing cleaning
  //
  bool  passCleaning   = true;
  if(!m_event->m_doTruthOnly)
    {
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jetcont(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}
    }

  //
  //  Jet Cleaning 
  //
  if(m_doCleaning && !passCleaning)
    {
      ANA_MSG_DEBUG(" Fail cleaning");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_cleaning, eventWeight);

  //
  // Dijet part of the selection
  //
  const xAH::Jet* jet0      =m_event->jetcont(0);
  const xAH::Jet* jet1      =m_event->jetcont(1);

  if(jet0->p4.Pt() < m_jet0PtCut)
    {
      ANA_MSG_DEBUG(" Fail JetPt0 ");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet0,eventWeight);

  if(jet1->p4.Pt() < m_jet1PtCut)
    {
      ANA_MSG_DEBUG(" Fail JetPt1");
      return EL::StatusCode::SUCCESS;
    }
  m_cutflow->execute(m_cf_jet1,eventWeight);

  //
  // Binning part of the selection
  //
  if(m_minPt>0)
    {
      if(jet0->p4.Pt() < m_minPt)
	{
	  ANA_MSG_DEBUG(" Fail min pt");
	  return EL::StatusCode::SUCCESS;
      }
      m_cutflow->execute(m_cf_minpt,eventWeight);
    }

  if(m_maxPt>0)
    {
      if(m_maxPt < jet0->p4.Pt()) 
	{
	  ANA_MSG_DEBUG(" Fail max pt");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_maxpt,eventWeight);
    }

  if(m_minEta>0)
    {
      if(jet0->p4.Eta() < m_minEta)
	{
	  ANA_MSG_DEBUG(" Fail min eta");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_mineta,eventWeight);
    }

  if(m_maxEta>0)
    {
      if(m_maxEta < jet0->p4.Eta())
	{
	  ANA_MSG_DEBUG(" Fail max eta");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_maxeta,eventWeight);
    }

  ANA_MSG_DEBUG(" Pass All Cuts");

  //
  //Filling
  hJet->execute(jet0, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetStudiesAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hJet->finalize());
  delete hJet;

  return EL::StatusCode::SUCCESS;
}

