#include <ZprimeDM/ZprimeAlgorithm.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <SampleHandler/MetaFields.h>

#include <xAODEventInfo/EventInfo.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

// this is needed to distribute the algorithm to the workers
ClassImp(ZprimeAlgorithm)

ZprimeAlgorithm :: ZprimeAlgorithm (const std::string& className)
: Algorithm(className),
  m_cutflowHist(0), m_cutflowHistW(0), m_cutflowHistOrig(0), m_cutflowHistWOrig(0)
{ }

EL::StatusCode ZprimeAlgorithm :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("ZprimeAlgorithm").ignore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeAlgorithm::copy_cutflow()
{

  TFile *fh_cutflow=wk()->getOutputFile("cutflow");
  fh_cutflow->cd();

  m_cutflowHistOrig  = m_cutflowHist;
  m_cutflowBinsOrig  = m_cutflowHist->GetNbinsX();
  m_cutflowHistWOrig = m_cutflowHistW;
  m_cutflowBinsWOrig = m_cutflowHistW->GetNbinsX();

  m_cutflowHist = dynamic_cast<TH1D*>(m_cutflowHist ->Clone((m_name + "_" + m_cutflowHist ->GetName()).c_str()));
  m_cutflowHistW= dynamic_cast<TH1D*>(m_cutflowHistW->Clone((m_name + "_" + m_cutflowHistW->GetName()).c_str()));

  return EL::StatusCode::SUCCESS;
}

int ZprimeAlgorithm::init_cutflow(const std::string& cutName)
{
  int binN=m_cutflowHist ->GetXaxis()->FindBin(cutName.c_str());
  int binW=m_cutflowHistW->GetXaxis()->FindBin(cutName.c_str());

  if(binN!=binW)
    std::cout << "Warning! nEvents and sumW bins don't match for " << cutName << std::endl;

  return binN;
}

EL::StatusCode ZprimeAlgorithm::cutflow(int cut)
{
  static SG::AuxElement::ConstAccessor< float >  mcEvtWeightAcc("mcEventWeight");

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store));

  float eventWeight(1);
  if( mcEvtWeightAcc.isAvailable( *eventInfo ) )
    eventWeight *= mcEvtWeightAcc( *eventInfo );

  m_cutflowHist ->Fill(cut, 1);
  m_cutflowHistW->Fill(cut, eventWeight );

  return EL::StatusCode::SUCCESS;
}

float ZprimeAlgorithm::event_weight() const
{
  static SG::AuxElement::ConstAccessor< float >  mcEvtWeightAcc("mcEventWeight");
  static SG::AuxElement::ConstAccessor< float >  weight_xsAcc("weight_xs");

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store));

  // Update the event weight, if not available
  // if(!weight_xsAcc.isAvailable( *eventInfo ))
  //   {
  //     double xs     =wk()->metaData()->castDouble(SH::MetaFields::crossSection    ,1);
  //     double eff    =wk()->metaData()->castDouble(SH::MetaFields::filterEfficiency,1);

  //     eventInfo->auxdecor< float >("weight_xs") = eff*xs; ///nEvents;
  //   }

  // Calculate the event weight
  float eventWeight(1);
  if( mcEvtWeightAcc.isAvailable(*eventInfo) )
    eventWeight *= mcEvtWeightAcc( *eventInfo );

  //if( eventInfo->isAvailable< float >( "weight_xs" ) ) 
  //{
  //eventWeight *= eventInfo->auxdecor< float >( "weight_xs" );
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ))
    {
      double xs  =wk()->metaData()->castDouble(SH::MetaFields::crossSection    ,1);
      double eff =wk()->metaData()->castDouble(SH::MetaFields::filterEfficiency,1);
      eventWeight*=xs*eff;
    }


  return eventWeight;
}

EL::StatusCode ZprimeAlgorithm :: histInitialize ()
{
  ANA_MSG_INFO("ZprimeAlgorithm::histInitialize()");
  ANA_CHECK(xAH::Algorithm::algInitialize());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeAlgorithm :: initialize ()
{
  ANA_MSG_INFO("ZprimeAlgorithm::initialize()");

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  TFile *fh_cutflow=wk()->getOutputFile("cutflow");
  m_cutflowHist  = dynamic_cast<TH1D*>(fh_cutflow->Get("cutflow"));
  m_cutflowHistW = dynamic_cast<TH1D*>(fh_cutflow->Get("cutflow_weighted"));
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeAlgorithm :: finalize () 
{
  if(m_cutflowHistOrig)
    {
      for(int bin=1;bin<m_cutflowBinsOrig;bin++)
  	m_cutflowHist->SetBinContent(bin,m_cutflowHistOrig->GetBinContent(bin));
    }
  if(m_cutflowHistWOrig)
    {
      for(int bin=1;bin<m_cutflowBinsWOrig;bin++)
  	m_cutflowHistW->SetBinContent(bin,m_cutflowHistWOrig->GetBinContent(bin));
    }

  return EL::StatusCode::SUCCESS; 
}

EL::StatusCode ZprimeAlgorithm :: histFinalize () {
  ANA_CHECK(xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZprimeAlgorithm::write_cutflow(const std::string m_fileName, const std::string histName)
{

  std::string thisName;
  std::stringstream m_ss;

  m_ss.str( std::string() );
  m_ss << histName;
  TFile * treeFile = wk()->getOutputFile( m_fileName );

  if(m_cutflowHist)
    {
      TH1F* thisCutflowHist = (TH1F*) m_cutflowHist->Clone();
      thisName = thisCutflowHist->GetName();
      thisCutflowHist->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHist->SetDirectory( treeFile );
    }

  if(m_cutflowHistW)
    {
      TH1F* thisCutflowHistW = (TH1F*) m_cutflowHistW->Clone();
      thisName = thisCutflowHistW->GetName();
      thisCutflowHistW->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHistW->SetDirectory( treeFile );
    }
  return EL::StatusCode::SUCCESS; 
}
