#include "ZprimeDM/ZprimeHelperClasses.h"

namespace ZprimeHelperClasses
{

  void DijetISRInfoSwitch::initialize()
  {
    m_truthz = has_exact("truthz");
    m_2d     = has_exact("2d");
    m_debug  = has_exact("debug");
  }
} // close namespace HelperClasses
