#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "ZprimeDM/ZprimeMiniTreeNew.h"

ZprimeMiniTreeNew :: ZprimeMiniTreeNew(xAOD::TEvent * event, TTree* tree, TFile* file) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("ZprimeMiniTreeNew", "Creating output TTree");
}

ZprimeMiniTreeNew :: ~ZprimeMiniTreeNew()
{
}
//////////////////// Connect Defined variables to branches here /////////////////////////////
void ZprimeMiniTreeNew::AddEventUser(const std::string& /*detailStr*/)
{
  m_tree->Branch("weight",    &m_weight   , "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");

  m_tree->Branch("Zprime_pt" , &m_Zprime_pt ,"Zprime_pt/F");
  m_tree->Branch("Zprime_eta", &m_Zprime_eta,"Zprime_eta/F");
  m_tree->Branch("Zprime_phi", &m_Zprime_phi,"Zprime_phi/F");
  m_tree->Branch("Zprime_m"  , &m_Zprime_m  ,"Zprime_m/F");
}

//////////////////// Clear any defined vectors here ////////////////////////////
void ZprimeMiniTreeNew::ClearEventUser() {
  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;

  m_Zprime_pt  = -999;
  m_Zprime_eta = -999;
  m_Zprime_phi = -999;
  m_Zprime_m   = -999;
}

/////////////////// Assign values to defined event variables here ////////////////////////
void ZprimeMiniTreeNew::FillEventUser( const xAOD::EventInfo* eventInfo ) {
  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );

  // truth Z' kinematic info
  if( eventInfo->isAvailable< float >( "Zprime_pt" ) )
    m_Zprime_pt = eventInfo->auxdecor< float >( "Zprime_pt" );
  if( eventInfo->isAvailable< float >( "Zprime_eta" ) )
    m_Zprime_eta = eventInfo->auxdecor< float >( "Zprime_eta" );
  if( eventInfo->isAvailable< float >( "Zprime_phi" ) )
    m_Zprime_phi = eventInfo->auxdecor< float >( "Zprime_phi" );
  if( eventInfo->isAvailable< float >( "Zprime_m" ) )
    m_Zprime_m = eventInfo->auxdecor< float >( "Zprime_m" );
}
