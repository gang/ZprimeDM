#ifndef ZprimeDM_TruthPhotonJetOR_H
#define ZprimeDM_TruthPhotonJetOR_H

// algorithm wrapper
#include <ZprimeDM/ZprimeAlgorithm.h>

#include <TH1D.h>

class TruthPhotonJetOR : public ZprimeAlgorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  std::string m_jetContainerName;    //!< JetContainerName - name of the input container with jets to select on
  std::string m_photonContainerName; //!< PhotonContainerName - name of the input container with photons to select on
  std::string m_outJetContainerName; //!< OutJetContainerName - name of the output container with selected jets

  double m_minDR;                    //!< MinDR - Minimum Delta R between photon and jet

private:
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

private:

public:
  // this is a standard constructor
  TruthPhotonJetOR (const std::string& className="TruthPhotonJetOR");

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TruthPhotonJetOR, 1);
};

#endif
