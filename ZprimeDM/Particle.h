#ifndef ZprimeDM_Particle_H
#define ZprimeDM_Particle_H

#include <TLorentzVector.h>

namespace ZprimeDM
{
  class Particle : public TObject
  {
    ClassDef(Particle, 1);
    
  public:

    Particle() : TObject() {};
    virtual ~Particle() {};

    TLorentzVector p4;
  };

} //ZprimeDM
#endif // ZprimeDM_Particle_H
