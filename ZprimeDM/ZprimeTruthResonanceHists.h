#ifndef ZprimeDM_ZprimeTruthResonanceHists_H
#define ZprimeDM_ZprimeTruthResonanceHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODJet/Jet.h>

#include <ZprimeDM/DijetISREvent.h>

class ZprimeTruthResonanceHists : public HistogramManager
{
public:
  ZprimeTruthResonanceHists(const std::string& name, const std::string& detailStr="");
  virtual ~ZprimeTruthResonanceHists();

  virtual StatusCode initialize();
  virtual StatusCode execute(const TLorentzVector& Zprime, float eventWeight) ;
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  //basic
  TH1F* h_zpt;
  TH1F* h_zeta;
  TH1F* h_zphi;
  TH1F* h_zm;
};

#endif
