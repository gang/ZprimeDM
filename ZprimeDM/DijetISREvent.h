#ifndef ZprimeDM_DijetISREvent_H
#define ZprimeDM_DijetISREvent_H

#include <TTree.h>
#include <TLorentzVector.h>
#include <TClonesArray.h>

#include <vector>
#include <string>

#include <xAODAnaHelpers/JetContainer.h>
#include <xAODAnaHelpers/PhotonContainer.h>
#include <xAODAnaHelpers/FatJetContainer.h>
#include <xAODAnaHelpers/TruthContainer.h>

#include <ZprimeDM/Jet.h>
#include <ZprimeDM/Photon.h>
#include <ZprimeDM/FatJet.h>
#include <ZprimeDM/TruthParticle.h>

class DijetISREvent
{
public:
  static DijetISREvent *global();

  virtual ~DijetISREvent();

  void setTree(TTree *t);
  void updateEntry();

  void setTriggerDetail(const std::string& detailStr);

  void initializeJets    (const std::string& name, const std::string& detailStr);
  void initializePhotons (const std::string& name, const std::string& detailStr);
  void initializeFatJets (const std::string& name, const std::string& detailStr);
  void initializeTruth   (const std::string& name, const std::string& detailStr);
  void initializeTrigJets(const std::string& name, const std::string& detailStr);

public:
  // Settings
  bool m_doTruthOnly;
  bool m_mc;

  // Event variables
  float m_ht;
  int m_NPV;
  float m_actualInteractionsPerCrossing;
  float m_averageInteractionsPerCrossing;
  int m_runNumber;
  long long m_eventNumber;
  int m_lumiBlock;

  int m_mcEventNumber;
  int m_mcChannelNumber;
  float m_mcEventWeight;

  // trigger
  std::vector<std::string> *m_passedTriggers;
  std::vector<float>       *m_triggerPrescales;

  // weights
  float m_weight;
  float m_weight_xs;
  float m_weight_pileup;

  // custom
  float m_Zprime_pt;
  float m_Zprime_eta;
  float m_Zprime_phi;
  float m_Zprime_m;

  TLorentzVector m_Zprime;

  // Particles
  bool haveJetsCont() const;
  uint jets() const;
  const ZprimeDM::Jet* jet(uint idx) const;
  const xAH::Jet*  jetcont(uint idx) const;

  bool havePhotonsCont() const;
  uint photons() const;
  const ZprimeDM::Photon* photon(uint idx) const;
  const xAH::Photon*  photoncont(uint idx) const;

  bool haveFatJetsCont() const;
  uint fatjets() const;
  const ZprimeDM::FatJet* fatjet(uint idx) const;
  const xAH::FatJet*  fatjetcont(uint idx) const;

  bool haveTrigJetsCont() const;
  uint trigjets() const;
  const ZprimeDM::Jet* trigjet(uint idx) const;
  const xAH::Jet*  trigjetcont(uint idx) const;

  bool haveTruthsCont() const;
  uint truths() const;
  const ZprimeDM::TruthParticle* truth(uint idx) const;
  const xAH::TruthPart*      truthcont(uint idx) const;

protected:
  DijetISREvent();

  TTree *m_tree;

  // particles
  TClonesArray *m_jets;
  TClonesArray *m_photons;
  TClonesArray *m_fatjets;
  TClonesArray *m_trigjets;
  TClonesArray *m_truths;

  // Cache of converted particles
  TClonesArray *m_trphotons;

  // containers (old style particles)
  xAH::JetContainer    *m_jets_container;
  xAH::PhotonContainer *m_photons_container;
  xAH::FatJetContainer *m_fatjets_container;
  xAH::TruthContainer  *m_truth_container;
  xAH::JetContainer    *m_trigJets_container;

private:
  static DijetISREvent *m_event;

  HelperClasses::TriggerInfoSwitch *m_triggerInfoSwitch;
};

#endif // ZprimeDM_DijetISREvent_H
