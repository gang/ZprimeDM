#ifndef ZprimeDM_DiphotonHists_H
#define ZprimeDM_DiphotonHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/EventHists.h>
#include <ZprimeDM/JetHists.h>
#include <ZprimeDM/PhotonHists.h>
#include <ZprimeDM/ResonanceHists.h>

class DiphotonHists : public EventHists
{
public:

  DiphotonHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& photonDetailStr);
  virtual ~DiphotonHists() ;

  virtual void record(EL::Worker *wk);

  virtual StatusCode initialize();
  virtual StatusCode execute(const DijetISREvent& event, const xAH::Photon* reso0, const xAH::Photon* reso1, float eventWeight);
  virtual StatusCode finalize();
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //histograms
  ResonanceHists     *m_reso;

  std::string m_jetDetailStr;
  TH1F*     h_nJet;
  ZprimeDM::JetHists *m_jet0;
  ZprimeDM::JetHists *m_jet1;
  ZprimeDM::JetHists *m_jet2;

  std::string m_photonDetailStr;
  TH1F*     h_nPhoton;
  ZprimeDM::PhotonHists *m_photon0;
  ZprimeDM::PhotonHists *m_photon1;
  ZprimeDM::PhotonHists *m_photon2;
};

#endif // ZprimeDM_DiphotonHists_H
