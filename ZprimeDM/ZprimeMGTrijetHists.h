#ifndef ZprimeDM_ZprimeMGTrijetHists_H
#define ZprimeDM_ZprimeMGTrijetHists_H

#include <xAODAnaHelpers/HistogramManager.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <ZprimeDM/ZprimeHelperClasses.h>
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/TruthHists.h>

class ZprimeMGTrijetHists : public HistogramManager
{
public:
  ZprimeMGTrijetHists(const std::string& name, const std::string& detailStr="");
  virtual ~ZprimeMGTrijetHists();

  virtual void record(EL::Worker *wk);

  virtual StatusCode initialize();
  virtual StatusCode execute(const xAH::TruthPart* reso0, const xAH::TruthPart* reso1, const xAH::TruthPart* isr, float eventWeight);
  virtual StatusCode finalize();
  using HistogramManager::book; // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // overload

private:
  ZprimeDM::TruthHists *m_jet0;
  ZprimeDM::TruthHists *m_jet1;
  ZprimeDM::TruthHists *m_jet2;

  TH1F* h_ptjj;
  TH1F* h_ptjj_l;
  TH1F* h_etajj;
  TH1F* h_phijj;
  TH1F* h_mjj;

  TH1F* h_dEtajj;
  TH1F* h_dPhijj;
  TH1F* h_dRjj;

  TH1F* h_yStarjj;
  TH1F* h_yBoostjj;
  TH1F* h_chijj;

  TH1F* h_asymjj;
  TH1F* h_vecasymjj;
  TH1F* h_projasymjj;

  TH1F* h_yStarISRjj;
  TH1F* h_yBoostISRjj;
  TH1F* h_chiISRjj;

  TH1F* h_dRISRclosej;
  TH1F* h_dEtaISRclosej;
  TH1F* h_dPhiISRclosej;

  TH1F* h_dRISRclosephij;
  TH1F* h_dEtaISRclosephij;
  TH1F* h_dPhiISRclosephij;

  TH1F *h_ht;
  TH1F *h_pt0isr;
  TH1F *h_pt1isr;
  TH1F *h_pt1pt0;

  TH1F* h_dRZBoost;
  TH1F* h_dEtaZBoost;
  TH1F* h_dPhiZBoost;

  TH1F* h_dRZBoostQ;
  TH1F* h_dEtaZBoostQ;
  TH1F* h_dPhiZBoostQ;
};

#endif
