import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

#trigger stuff
singlejettrig = 'HLT_j[0-9]*.*'
#singlejettrig = ''
multijettrig = 'HLT_3j200 | HLT_j110'
httrig = 'HLT_ht1000_L1J100 | HLT_j110'
twobplusonejtrig = 'HLT_j100_2j55_bmv2c2060_split | HLT_j60'
twobplushttrig = 'HLT_2j55_bmv2c2060_split_ht300_L14J15 | HLT_j60'

c = Config()

jetDetailStr="kinematic clean energy layer trackPV flavTag"
for btagger in commonconfig.btaggers:
    jetDetailStr+=' jetBTag_{btagger}_HybBEff_{btagWP}'.format(btagger=btagger,btagWP=''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
if args.is_MC: jetDetailStr+=" truth"

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 #triggerSelection='L1_J[0-9]*|L1_[0-9]J[0-9]*|HLT_j[0-9]*.*|HLT_noalg_J[0-9]*|HLT_[0-9]j*.*|HLT_ht[0-9]*.*', #old! too broad
                                 triggerSelection=singlejettrig+' | '+multijettrig+' | '+httrig+' | '+twobplusonejtrig+' | '+twobplushttrig,
                                 doJets=True,btagWPs=commonconfig.btagWPs,
                                 doPhotons=False)

containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET   ,'SignalJets'   ,'jet',jetDetailStr                          ,'kinematic clean'))

c.algorithm("ZprimeNtupler",          { "m_name"                : "AnalysisAlgo",
                                        "m_inputAlgo"           : "SignalJets_Algo",
                                        "m_containers"          : containers,
                                        "m_eventDetailStr"      : "pileup",
                                        "m_trigDetailStr"       : "passTriggers passTrigBits"
                                        } )
