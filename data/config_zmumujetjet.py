import ROOT
from xAODAnaHelpers import Config

c = Config()

c.algorithm("BasicEventSelection",          { "m_truthLevelOnly"      : True,
                                              "m_derivationName"      : "TRUTH1",
                                              "m_useMetaData"         : False
                                              } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "AntiKt4TruthJets",
                                       "m_outContainerName"        :  "AntiKt4TruthJetsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "TruthPhotons",
                                       "m_outContainerName"        :  "TruthPhotonsSort"
                                       } )

c.algorithm("SortAlgo",              { "m_inContainerName"         :  "TruthMuons",
                                       "m_outContainerName"        :  "TruthMuonsSort"
                                       } )

c.algorithm("JetSelector",                  { "m_inContainerName"         :  "AntiKt4TruthJetsSort",
                                              "m_outContainerName"        :  "SignalJets",
                                              "m_decorateSelectedObjects" :  False, 
                                              "m_createSelectedContainer" :  True, 
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.8,
                                              } )

c.algorithm("TruthSelector",                { "m_inContainerName"         :  "TruthPhotonsSort",
                                              "m_outContainerName"        :  "SignalPhotons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True,
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.5,
                                              } )

c.algorithm("TruthSelector",                { "m_inContainerName"         :  "TruthMuonsSort",
                                              "m_outContainerName"        :  "SignalMuons",
                                              "m_decorateSelectedObjects" :  False,
                                              "m_createSelectedContainer" :  True,
                                              "m_pT_min"                  :  25e3,
                                              "m_eta_max"                 :  2.4,
                                              } )

c.algorithm("TruthPhotonJetOR",             { "m_jetContainerName"    : "SignalJets",
                                              "m_photonContainerName" : "SignalMuons",
                                              "m_outJetContainerName" : "SignalJetswoMuon",
                                              "m_minDR"               : 0.4
                                              } )

                                         
c.algorithm("ZprimeZmumuJetJetHistsAlgo",   { "m_name"                : "zmumujetjet_HLT_zmumu",
                                              "m_jetContainerName"    : "SignalJetswoMuon",
                                              "m_photonContainerName" : "SignalPhotons",
                                              "m_muonContainerName"   : "SignalMuons",
                                              "m_minJetPt"            : 25,
                                              "m_minMuonPt"           : 25,
                                              } )
