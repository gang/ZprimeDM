import ROOT
from xAODAnaHelpers import Config

histDetail= ""
jetDetail ="kinematic energy clean trackPV charge"

commonsel={"m_mc"                     : args.is_MC,
           "m_debug"                  : False,
           "m_histDetailStr"          : histDetail,
           "m_jetDetailStr"           : jetDetail,
           "m_doDetails"              : True,
           "m_doPUReweight"           : True,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           "m_doTrigger"              : True,
           }

c = Config()

GRL = "GoodRunsLists/data15_13TeV/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data16_13TeV/data16_13TeV.periodAllYear_DetStatus-v79-pro20-05_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_debug"                  : False,
                                        "m_mc"                     : args.is_MC,
                                        "m_GRLxml"                 : GRL,
                                        "m_applyGRL"               : not args.is_MC,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetail
                                        } )


trijet_j0_2j25_num=commonsel.copy()
trijet_j0_2j25_num.update({"m_name"            : "trijet_j0_2j25_num",
                           "m_trigger"         : "HLT_j360,HLT_j380",
                           "m_reso0PtCut"      : 25,
                           "m_reso1PtCut"      : 25,
                           "m_minLeadingJetPt" : 0,
                           "m_YStarCut"        : 0.6})
c.algorithm("ZprimeTrijetHistsAlgo", trijet_j0_2j25_num )

trijet_j0_2j25_den=commonsel.copy()
trijet_j0_2j25_den.update({"m_name"            : "trijet_j0_2j25_den",
                           "m_trigger"         : "HLT_j360",
                           "m_reso0PtCut"      : 25,
                           "m_reso1PtCut"      : 25,
                           "m_minLeadingJetPt" : 0,
                           "m_YStarCut"        : 0.6})
c.algorithm("ZprimeTrijetHistsAlgo", trijet_j0_2j25_den )
