import ROOT
from xAODAnaHelpers import Config

c = Config()

#
# Process Ntuple
#

c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_debug"                  : False,
                                        "m_mc"                     : True,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : True,
                                        "m_truthDetailStr"         : "kinematic parents"
                                        } )

c.algorithm("ZprimeMGTrijetAlgo", { "m_name"           : "leadjet430",
                                    "m_debug"          : False,
                                    "m_jetPtCut"       : 25,
                                    "m_leadJetPtCut"   : 430,
                                    "m_truthDetailStr" : "kinematic"
                                    } )

pickModes=['truth','m12','m13','m23','dPhimin','dPhimax','dRmin','dRmax','dEtamin','dEtamax','etasort','minystar']
#pickModes=['truth']
for pickMode in pickModes:
    c.algorithm("ZprimeMGTrijetPickAlgo", { "m_name"           : "leadjet430/pick_%s"%pickMode,
                                            "m_debug"          : False,
                                            "m_pickMode"       : pickMode,
                                            "m_jetPtCut"       : 25,
                                            "m_leadJetPtCut"   : 430,
                                            "m_truthDetailStr" : "kinematic"
                                            } )
